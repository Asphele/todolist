<header>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
<link rel="stylesheet" href="lib/style.css">
<?php include "navbar.php"; ?>
</header>
<div id="myDIV" class="header">
  <h2 style="margin:5px">My To Do List</h2>
  

  <button onclick="document.getElementById('id02').style.display='block'" class="w3-button w3-green w3-large">Add List</button>
</div>

<?php
  include "connection.php";
  include "createModal.php";
  $sql = ("SELECT * FROM `lists`");
  $query = $conn->prepare($sql);
  $query->execute();

  $result = $query->fetchAll(PDO::FETCH_ASSOC);



?>
<form >
<ul id="myUL">
<?php foreach ($result as $row) { ?>
    <li><?php echo $row['list_name']; ?> <span class="edit_task"><a href="listdelete.php?id=<?php echo $row['list_id']; ?>">Delete </a></span><span class="delete_task"><a href="listeditModal.php?id=<?php echo $row['list_id']; ?>"> Edit </a></span></li>
<?php } ?>
</ul>
</form>
<script src="lib/script.js"></script>