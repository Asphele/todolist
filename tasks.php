<header>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
<link rel="stylesheet" href="lib/style.css">

</header>

<div id="myDIV" class="header">
<?php include "navbar.php";?>
  <h2 style="margin:5px">My Tasks</h2>

</div>
<button onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-green w3-large">Add Task</button>
<?php
include "connection.php";
include "createModal.php";
$sql = "SELECT * FROM `tasks`";
$result = $conn->query($sql);



?>
<form >
<ul id="myUL">
<?php while($row = $result->fetch(PDO::FETCH_ASSOC)) {?> 
  <li><?php echo $row['task_name']; ?> <span class="edit_task"><a href="delete.php?id=<?php echo $row['task_id']; ?>">Delete </a></span><span class="delete_task"><a href="editModal.php?id=<?php echo $row['task_id']; ?>"> Edit </a></span></li>
    
<?php }?>
</ul>
</form>
