<?php 
  $sql = ("SELECT * FROM `tasks`");
  $query = $conn->prepare($sql);
  $query->execute();

  $result = $query->fetchAll(PDO::FETCH_ASSOC);

?>

<div class="w3-container">  
<!-- Create Task -->
  <div id="id01" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
      <div class="w3-center"><br>
        <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
      </div>
      <form class="w3-container" method="post" action="action.php" name="create_task">
        <div class="w3-section">
            <h1>Create Task</h1>
          <label><b>Task Name</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter taskname" name="task_name" required>
          <button class="w3-button w3-block w3-green w3-section w3-padding" name="create_task" type="submit">Create</button>
        </div>
      </form>
    </div>
  </div>




<!-- Create list -->
  <div id="id02" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

      <div class="w3-center"><br>
        <span onclick="document.getElementById('id02').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
      </div>
      <form class="w3-container" method="post" action="action.php" name="create_list">
        <div class="w3-section">
            <h1>Create List</h1>
          <label><b>Name</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter name" name="list_name" required><br>

          <label><b>Task</b></label>
          <select name="option1">
          <?php foreach ($result as $row) {?>
            <option><?php echo $row['task_name']; ?></option>
          <?php }?>
          </select><br>

          <label><b>Task</b></label>
          <select name="option2">
          <?php foreach ($result as $row) {?>
            <option><?php echo $row['task_name']; ?></option>
          <?php }?>
          </select><br>

          <label><b>Task</b></label>
          <select name="option3">
          <?php foreach ($result as $row) {?>
            <option><?php echo $row['task_name']; ?></option>
          <?php }?>
          </select><br>

          
          
          <button class="w3-button w3-block w3-green w3-section w3-padding" name="create_list" type="submit">Create</button>
        </div>
      </form>
    </div>
  </div>


</div>