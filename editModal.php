<?php
include "connection.php";
$id = $_GET['id'];


$sql = "SELECT * FROM `tasks` WHERE task_id = $id";
$result = $conn->query($sql);
$row = $result->fetch(PDO::FETCH_ASSOC);

?>



<div class="w3-container">
  <div id="id02" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

      <div class="w3-center"><br>
        <span onclick="document.getElementById('id02').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
      </div>
      <form class="w3-container" method="post" action="action.php" name="edit_task">
        <div class="w3-section">
            <h1>Edit Task</h1>
          <label><b>Task Name</b></label>
          <input type="hidden" name="id" value="<?php echo $row["task_id"]; ?>"/>
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="<?php echo $row['task_name'];?>" name="task_name" required>
          <button class="w3-button w3-block w3-green w3-section w3-padding" name="task_edit" type="submit">Save</button>
          
        </div>
      </form>
     

    </div>
  </div>
</div>