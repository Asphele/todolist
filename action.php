<?php
include "connection.php";

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['id'];
      if(isset($_POST["create_task"])){
        $name = input($conn, $_POST["task_name"]);
      }else if(isset($_POST['task_edit'])){
        $edit = edit($conn, $_POST["task_name"], $id);
      }else if(isset($_POST['task_delete'])){
        $delete = delete($conn, $id);
      }

      if(isset($_POST["create_list"])){
        $name = $_POST["list_name"];
        $list_task = [$_POST['option1'],$_POST['option2'],$_POST['option3']];
        
        $stmt = $conn->prepare("INSERT INTO lists(list_name, task_one, task_two, task_three) values (:name, :taskOne, :taskTwo, :taskThree)");
        $stmt->bindParam(':name', $name, PDO::PARAM_STR, 255);
        $stmt->bindParam(':taskOne', $list_task[0], PDO::PARAM_STR, 255);
        $stmt->bindParam(':taskTwo', $list_task[1], PDO::PARAM_STR, 255);
        $stmt->bindParam(':taskThree', $list_task[2], PDO::PARAM_STR, 255);
        $stmt->execute();
        
       
      }

      header("Location: index.php");
      
}

function validate($data){
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);

  return $data;
}

 function input($conn, $data) {
   validate($data);
    
    $stmt = $conn->prepare("INSERT INTO tasks(task_name) values (:name)");
        $stmt->bindParam(':name', $data, PDO::PARAM_STR, 255);
        $stmt->execute();

  }

  function edit($conn, $data, $id){
    validate($data);

    $stmt = $conn->prepare("UPDATE tasks SET task_name='$data' WHERE task_id=$id");
    $stmt->bindParam(':name', $data, PDO::PARAM_STR, 255);
    $stmt->execute();
  }

  function delete($conn, $id){
    $stmt = $conn->prepare("DELETE FROM tasks WHERE task_id=$id");
    $stmt->execute();
  }

 


  ?>